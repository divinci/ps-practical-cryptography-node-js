# Practical Cryptography in Node.js

Project developed during the [Pluralsight - Practical Cryptography in Node.js](https://www.pluralsight.com/courses/practical-cryptography-node-js) course.

Running the project:
 
1. Open the directory: `cd Sample-2FA`.
 
2. Install dependencies: `npm install`.
 
3. Run the application: `node server`.
 
4. Open the browser and access http://localhost:3000/app/2fa.
 
5. Using Google Authenticator, scan the QR Code generated by speakeasy library.

6. Access http://localhost:3000/app/authenticate

7. Type the secret key generated by Google Authenticator and submit the form. 